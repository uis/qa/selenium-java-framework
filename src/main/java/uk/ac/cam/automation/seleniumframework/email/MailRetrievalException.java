package uk.ac.cam.automation.seleniumframework.email;

public class MailRetrievalException extends RuntimeException {

    public MailRetrievalException(String message) {
        super(message);
    }

    public MailRetrievalException(String message, Throwable e) {
        super(message, e);
    }

}