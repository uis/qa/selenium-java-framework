package uk.ac.cam.automation.seleniumframework.driver;

public enum BrowserType {

    ChromeLocal,
    ChromeLocalNoSandbox,
    ChromeRemote,
    AppiumWindowsTenRemote,
    AppiumMobile,
    EdgeLocal,
    EdgeRemote,
    FirefoxLocal,
    FirefoxLocalDebug,
    FirefoxRemote,
    FirefoxEagerLoadRemote,
    FirefoxRemoteDebug,
    InternetExplorerLocal,
    InternetExplorerNativeEventsRemote,
    InternetExplorerRemote,
    SafariLocal,
    SafariRemote,
    SafariTechPreviewLocal,
    SafariTechPreviewRemote
}