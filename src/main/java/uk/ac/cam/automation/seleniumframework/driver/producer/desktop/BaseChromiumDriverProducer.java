package uk.ac.cam.automation.seleniumframework.driver.producer.desktop;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import uk.ac.cam.automation.seleniumframework.driver.producer.BaseDriver;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseChromiumDriverProducer extends BaseDriver {

    protected ChromeOptions getBaseChromeOptions() {
        ChromeOptions chromeOptions = new ChromeOptions();
        for (Map.Entry<String, Object> entry : getBaseBrowserOptions().entrySet()) {
            chromeOptions.setCapability(entry.getKey(), entry.getValue());
        }
        return chromeOptions;
    }

    protected ChromeOptions getDefaultLocalChromiumOptions() {
        ChromeOptions chromeOptions = getBaseChromeOptions();
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--verbose");
        chromeOptions.addArguments("--disable-dev-shm-usage");
        chromeOptions.addArguments("--safebrowsing-disable-download-protection");
        chromeOptions.addArguments("--lang=en_GB.UTF-8");
        chromeOptions.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);

        Map<String, Object> prefs = new HashMap<>();
        prefs.put("safebrowsing.enabled", "true");
        chromeOptions.setExperimentalOption("prefs", prefs);

        return chromeOptions;
    }

    protected ChromeOptions getDefaultRemoteChromiumOptions() {
        ChromeOptions chromeOptions = this.getDefaultLocalChromiumOptions();
        if (browserVersion != null) {
            chromeOptions.setCapability("browserVersion", browserVersion);
        }
        if (getBrowserstackOptions() != null) {
            chromeOptions.setCapability("bstack:options", getBrowserstackOptions());
        }
        return chromeOptions;
    }

}
