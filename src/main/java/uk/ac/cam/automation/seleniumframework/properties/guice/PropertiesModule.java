package uk.ac.cam.automation.seleniumframework.properties.guice;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import uk.ac.cam.automation.seleniumframework.properties.PropertiesFileNotFoundException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesModule extends AbstractModule {

    @Override
    protected void configure() {
        InputStream is = getClass().getClassLoader().getResourceAsStream("properties/test-automation.properties");
        if (is != null) {
            try {
                Properties properties = new Properties();
                properties.load(is);
                Names.bindProperties(binder(), properties);
            } catch (IOException e) {
                throw new PropertiesFileNotFoundException(e);
            }
        }
    }
}