package uk.ac.cam.automation.seleniumframework.properties;

import uk.ac.cam.automation.seleniumframework.log.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class PropertyLoader {

    private static final Set<PropertiesFileConfig> propertiesFileConfigs;

    static {
        propertiesFileConfigs = new HashSet<>();
        registerPropertiesFileConfig(new PropertiesFileConfig("properties/test-automation.properties"));
    }

    /**
     * Tries to look up a property using System.getProperty() as a priority and then from any properties files that have been registered with this PropertyLoader.
     * <p>
     * By default, the properties file <strong>properties/environments.properties</strong> is registered.
     *
     * @param propertyKey the property to search for the value of
     * @return the property relating to the propertyKey or null if nothing was found
     */
    public static String getProperty(String propertyKey) {
        for (String propertyName: getValidNamesForProperty(propertyKey)) {
            if (System.getProperty(propertyName) != null) {
                Log.Debug("Found property, " + propertyName + " as a system property of " + System.getProperty(propertyName));
                return System.getProperty(propertyName);
            }

            for (PropertiesFileConfig config : propertiesFileConfigs) {
                String property = getValueFromPropertiesFile(config, propertyName);

                if (property != null) {
                    Log.Debug("Found property, " + propertyName + " in file: " + config.getFileName() + " with value of " + property);
                    return property;
                }
            }
        }

        return null;
    }

    /**
     * Register additional properties files with the PropertyLoader class
     *
     */
    public static void registerPropertiesFileConfig(PropertiesFileConfig propertiesFileConfig) {
        propertiesFileConfigs.add(propertiesFileConfig);
    }

    private static String getValueFromPropertiesFile(PropertiesFileConfig config, String propertyKey) {
        Properties properties = new Properties();
        InputStream inputStream;

        try {
            inputStream = PropertyLoader.class.getClassLoader().getResourceAsStream(config.getFileName());
            if (inputStream == null) {
                System.out.println("Sorry, unable to find " + config.getFileName());
                return null;
            }
            Log.Debug("Loaded resource from " + Objects.requireNonNull(PropertyLoader.class.getClassLoader().getResource(config.getFileName())).getPath());
            properties.load(inputStream);
            return properties.getProperty(propertyKey);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private static List<String> getValidNamesForProperty(String propertyName){
        String cobolCase = propertyName.replace(".", "_").toUpperCase();
        String dotNotation = propertyName.replace("_", ".").toLowerCase();
        return Arrays.asList(cobolCase, dotNotation);
    }

}
