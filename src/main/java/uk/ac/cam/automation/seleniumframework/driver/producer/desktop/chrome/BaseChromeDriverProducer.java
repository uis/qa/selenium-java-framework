package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.chrome;

import org.openqa.selenium.chrome.ChromeOptions;
import uk.ac.cam.automation.seleniumframework.driver.producer.desktop.BaseChromiumDriverProducer;


class BaseChromeDriverProducer extends BaseChromiumDriverProducer {
    ChromeOptions getDefaultLocalOptions() {
        return new ChromeOptions().merge(getDefaultLocalChromiumOptions());
    }

    ChromeOptions getDefaultRemoteOptions() {
        return new ChromeOptions().merge(getDefaultRemoteChromiumOptions());
    }

}
