package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.edge;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import uk.ac.cam.automation.seleniumframework.driver.GridUtils;
import uk.ac.cam.automation.seleniumframework.driver.producer.WebDriverProducer;

public class EdgeWebDriverProducer extends BaseEdgeDriverProducer implements WebDriverProducer {

    @Override
    public WebDriver produce() {
        RemoteWebDriver remoteWebDriver = new RemoteWebDriver(GridUtils.getSeleniumGridURL(), getDefaultRemoteOptions());
        remoteWebDriver.setFileDetector(new LocalFileDetector());
        return remoteWebDriver;
    }

}