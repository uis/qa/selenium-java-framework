package uk.ac.cam.automation.seleniumframework.driver;

import org.openqa.selenium.WebDriver;
import uk.ac.cam.automation.seleniumframework.driver.producer.WebDriverProducer;
import uk.ac.cam.automation.seleniumframework.driver.producer.desktop.chrome.*;
import uk.ac.cam.automation.seleniumframework.driver.producer.desktop.edge.EdgeLocalWebDriverProducer;
import uk.ac.cam.automation.seleniumframework.driver.producer.desktop.edge.EdgeWebDriverProducer;
import uk.ac.cam.automation.seleniumframework.driver.producer.desktop.firefox.*;
import uk.ac.cam.automation.seleniumframework.driver.producer.desktop.safari.SafariLocalWebDriverProducer;
import uk.ac.cam.automation.seleniumframework.driver.producer.desktop.safari.SafariWebDriverProducer;
import uk.ac.cam.automation.seleniumframework.driver.producer.desktop.safari.SafariTechPreviewLocalWebDriverProducer;
import uk.ac.cam.automation.seleniumframework.driver.producer.desktop.safari.SafariTechPreviewWebDriverProducer;
import uk.ac.cam.automation.seleniumframework.driver.producer.desktop.windows.WindowsTenRemoteDriverProducer;
import uk.ac.cam.automation.seleniumframework.driver.producer.mobile.appium.AppiumMobileWebDriverProducer;
import uk.ac.cam.automation.seleniumframework.log.Log;

import java.util.HashMap;
import java.util.Map;

class DriverFactory {

    private static final Map<BrowserType, WebDriverProducer> producers = new HashMap<>();

    static {
        producers.put(BrowserType.ChromeLocal, new ChromeLocalWebDriverProducer());
        producers.put(BrowserType.ChromeLocalNoSandbox, new ChromeLocalNoSandboxWebDriverProducer());
        producers.put(BrowserType.ChromeRemote, new ChromeWebDriverProducer());
        producers.put(BrowserType.AppiumWindowsTenRemote, new WindowsTenRemoteDriverProducer());
        producers.put(BrowserType.AppiumMobile, new AppiumMobileWebDriverProducer());
        producers.put(BrowserType.EdgeLocal, new EdgeLocalWebDriverProducer());
        producers.put(BrowserType.EdgeRemote, new EdgeWebDriverProducer());
        producers.put(BrowserType.FirefoxLocal, new FirefoxLocalWebDriverProducer());
        producers.put(BrowserType.FirefoxLocalDebug, new FirefoxLocalDebugWebDriverProducer());
        producers.put(BrowserType.FirefoxRemote, new FirefoxWebDriverProducer());
        producers.put(BrowserType.FirefoxEagerLoadRemote, new FirefoxEagerLoadWebDriverProducer());
        producers.put(BrowserType.FirefoxRemoteDebug, new FirefoxDebugWebDriverProducer());
        producers.put(BrowserType.SafariLocal, new SafariLocalWebDriverProducer());
        producers.put(BrowserType.SafariRemote, new SafariWebDriverProducer());
        producers.put(BrowserType.SafariTechPreviewLocal, new SafariTechPreviewLocalWebDriverProducer());
        producers.put(BrowserType.SafariTechPreviewRemote, new SafariTechPreviewWebDriverProducer());
    }

    private final BrowserType browserType;

    DriverFactory(String browserTypeName) {
        browserType = BrowserType.valueOf(browserTypeName);
        Log.Info("Browser type: " + browserType + " has been set in the Driver Factory on thread " + Thread.currentThread().getName());
    }

    WebDriver createInstance() {
        return producers.get(browserType).produce();
    }

}
