package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.firefox;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import uk.ac.cam.automation.seleniumframework.driver.producer.WebDriverProducer;

public class FirefoxLocalWebDriverProducer extends BaseFirefoxDriverProducer implements WebDriverProducer {

    @Override
    public WebDriver produce() {
        return new FirefoxDriver(getDefaultLocalOptions());
    }

}
