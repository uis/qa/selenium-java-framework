package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.chrome;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import uk.ac.cam.automation.seleniumframework.driver.producer.WebDriverProducer;

public class ChromeLocalNoSandboxWebDriverProducer extends BaseChromeDriverProducer implements WebDriverProducer {

    @Override
    public WebDriver produce() {
        ChromeOptions chromeOptions = getDefaultLocalOptions();
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--disable-dev-shm-usage");

        return new ChromeDriver(chromeOptions);
    }

}
