package uk.ac.cam.automation.seleniumframework.jsf.primefaces;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import uk.ac.cam.automation.seleniumframework.site.Site;

public class PrimefacesDate extends BasePrimefacesUtil {

    public static void select(String dateComponentId, String value) {
        String dateInputId = dateComponentId + "_input";
        String xpath = "//div[starts-with(@class,\"ui-datepicker\")][1]";
        Site.click(By.id(dateInputId));
        Site.webDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
        Site.enterTextBoxDetails(By.id(dateComponentId + "_input"), value);
        Site.findElement(By.id(dateInputId)).sendKeys(Keys.TAB);
        Site.findElement(By.id(dateInputId)).sendKeys(Keys.ESCAPE);
        try {
            Site.webDriverWait(2).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpath)));
        } catch (TimeoutException e) {
            Site.findElement(By.id(dateInputId)).sendKeys(Keys.ESCAPE);
            Site.webDriverWait().until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpath)));
        }
    }

}
