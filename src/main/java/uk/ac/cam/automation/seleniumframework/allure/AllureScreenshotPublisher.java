package uk.ac.cam.automation.seleniumframework.allure;

import io.qameta.allure.listener.StepLifecycleListener;
import io.qameta.allure.model.StepResult;
import uk.ac.cam.automation.seleniumframework.screenshots.ScreenshotTaker;


public class AllureScreenshotPublisher implements StepLifecycleListener {

    @Override
    public void afterStepUpdate(StepResult result) {
        ScreenshotTaker.attachScreenshot();
    }

}