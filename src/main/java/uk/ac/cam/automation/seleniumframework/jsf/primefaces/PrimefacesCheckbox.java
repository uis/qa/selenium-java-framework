package uk.ac.cam.automation.seleniumframework.jsf.primefaces;

import org.openqa.selenium.By;
import uk.ac.cam.automation.seleniumframework.site.Site;

public class PrimefacesCheckbox {

    public static void select(String label) {
        Site.click(By.xpath("(//label[normalize-space(text())=\"" + label + "\"])[1]"));
    }
}
