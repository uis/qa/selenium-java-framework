package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.firefox;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxDriverLogLevel;
import org.openqa.selenium.firefox.FirefoxOptions;
import uk.ac.cam.automation.seleniumframework.driver.producer.WebDriverProducer;

public class FirefoxLocalDebugWebDriverProducer extends BaseFirefoxDriverProducer implements WebDriverProducer {

    @Override
    public WebDriver produce() {
        FirefoxOptions firefoxOptions = getDefaultLocalOptions();
        firefoxOptions.setLogLevel(FirefoxDriverLogLevel.TRACE);
        return new FirefoxDriver(firefoxOptions);
    }

}
