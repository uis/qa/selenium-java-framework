package uk.ac.cam.automation.seleniumframework.screenshots;

import io.qameta.allure.Allure;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.RemoteWebDriver;
import uk.ac.cam.automation.seleniumframework.driver.DriverManager;
import uk.ac.cam.automation.seleniumframework.properties.CommonProperties;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ScreenshotTaker {
    private static final boolean isScreenshotOff = Boolean.parseBoolean(PropertyLoader.getProperty(CommonProperties.SCREENSHOT_OFF));

    public static void attachScreenshot(String screenshotName) {
        if (!isScreenshotOff && (DriverManager.getDriver() instanceof RemoteWebDriver)) {
            Allure.getLifecycle().addAttachment(
                    screenshotName,
                    "image/png",
                    "png",
                    getWebDriverScreenshot()
            );
        }
    }

    public static void attachScreenshot() {
        attachScreenshot(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MMM-yy_HH:mm:ss")));
    }


    private static byte[] getWebDriverScreenshot() {
        return (((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.BYTES));
    }

}
