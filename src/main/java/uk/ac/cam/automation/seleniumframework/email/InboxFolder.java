package uk.ac.cam.automation.seleniumframework.email;


import jakarta.mail.Folder;
import jakarta.mail.MessagingException;

record InboxFolder(Folder inbox) {

    public Folder getInbox() {
        return inbox;
    }

    public int getNewMessageCount() {
        try {
            return inbox.getNewMessageCount();
        } catch (MessagingException e) {
            throw new MailRetrievalException("Could not get message count from inbox", e);
        }
    }
}