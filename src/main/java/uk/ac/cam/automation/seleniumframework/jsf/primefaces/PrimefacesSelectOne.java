package uk.ac.cam.automation.seleniumframework.jsf.primefaces;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import uk.ac.cam.automation.seleniumframework.site.Site;


public class PrimefacesSelectOne extends BasePrimefacesUtil {

    public static void select(String selectoneId, String value) {
        openAndWaitForOptionListToAppear(selectoneId);
        clickPrimefacesSelectItem(value);
    }

    public static void selectByIndex(String selectoneId, int index) {
        int xpathInt = index + 1;
        openAndWaitForOptionListToAppear(selectoneId);
        String optionItemAtIndex = Site.findElement(By.xpath("(//div[contains(@style,\"display: block\")]//*[contains(@class,\"selectonemenu-item \") and not(contains(@id,(\"label\")))])[" + xpathInt + "]")).getText();
        clickPrimefacesSelectItem(optionItemAtIndex);
    }

    private static void openAndWaitForOptionListToAppear(String selectoneId) {
        Site.click(By.id(selectoneId + "_label"));
        Site.webDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.id(selectoneId + "_panel")));
        Site.webDriverWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//*[contains(@class,\"ui-selectonemenu-item \")]/ancestor::div[contains(@class,\"ui-selectonemenu-panel\") and not(contains(@style,\"none\"))])[1]")));
    }

    private static void clickPrimefacesSelectItem(String item) {
        String optionXpath = "//div[contains(@id,\"_panel\") and not(contains(@style,\"opacity:\")) and (contains(@style,\"display: block\") or contains(@style,\"display:block\"))]//*[normalize-space(@data-label)=\"" + item + "\"][1]";
        Site.webDriverWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath(optionXpath)));
        Site.click(By.xpath(optionXpath));
        Site.webDriverWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//label[normalize-space(string())=\"" + item + "\"][1]")));
    }

}
