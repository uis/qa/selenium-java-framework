package uk.ac.cam.automation.seleniumframework.file;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

public class FileReader {

    /**
     * Loads a resource from the class loader that loaded this class which will normally be the same ClassLoader that you want!
     *
     * @param path
     * @return
     * @throws IOException
     */
    public static String readFileFromResources(String path) throws IOException {
        return readFileFromResources(path, FileReader.class.getClassLoader());
    }

    /**
     * Loads a resource from a specified class loader.
     *
     * @param path
     * @param classLoader
     * @return
     * @throws IOException
     */
    public static String readFileFromResources(String path, ClassLoader classLoader) throws IOException {
        InputStream inputStream = classLoader.getResourceAsStream(path);
        if (inputStream == null) {
            throw new IOException("Resource not found: " + path);
        }
        return readFileToString(inputStream);
    }

    private static String readFileToString(InputStream inputStream) throws IOException {
        char[] buffer = new char[1024];
        int charsRead;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream)); StringWriter writer = new StringWriter()) {
            while ((charsRead = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, charsRead);
            }
            return writer.toString();
        }
    }

}
