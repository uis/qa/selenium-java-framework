package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.firefox;

import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import uk.ac.cam.automation.seleniumframework.driver.producer.BaseDriver;

import java.util.Map;


public class BaseFirefoxDriverProducer extends BaseDriver {

    protected FirefoxOptions getBaseFirefoxOptions() {
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        for (Map.Entry<String, Object> entry : getBaseBrowserOptions().entrySet()) {
            firefoxOptions.setCapability(entry.getKey(), entry.getValue());
        }
        return firefoxOptions;
    }

    FirefoxOptions getDefaultLocalOptions() {
        FirefoxOptions firefoxOptions = getBaseFirefoxOptions();
        firefoxOptions.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        firefoxOptions.addPreference("security.warn_submit_secure_to_insecure", false);
        // Disables scroll bar appearing:
        // causes issues with driver clicking this instead of the intended target
        firefoxOptions.addPreference("widget.gtk.alt-theme.scrollbar_active", false);
        firefoxOptions.addPreference("widget.gtk.overlay-scrollbars.enabled", false);
        firefoxOptions.addPreference("layout.css.scrollbar-gutter.enabled", false);
        firefoxOptions.addPreference("widget.windows.overlay-scrollbars.enabled", false);
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxProfile.setPreference("intl.accept_languages", "en-GB");
        firefoxOptions.setProfile(firefoxProfile);
        if (browserVersion != null) {
            firefoxOptions.setCapability("browserVersion", browserVersion);
        }
        return firefoxOptions;
    }

    FirefoxOptions getDefaultRemoteOptions() {
        FirefoxOptions firefoxOptions = this.getDefaultLocalOptions();
        if (getBrowserstackOptions() != null) {
            firefoxOptions.setCapability("bstack:options", getBrowserstackOptions());
        }
        return firefoxOptions;
    }


}
