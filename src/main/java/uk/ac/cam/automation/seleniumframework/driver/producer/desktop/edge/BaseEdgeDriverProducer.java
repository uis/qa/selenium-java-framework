package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.edge;

import org.openqa.selenium.edge.EdgeOptions;
import uk.ac.cam.automation.seleniumframework.driver.producer.desktop.BaseChromiumDriverProducer;


class BaseEdgeDriverProducer extends BaseChromiumDriverProducer {

    EdgeOptions getDefaultLocalOptions() {
        return new EdgeOptions().merge(getDefaultLocalChromiumOptions());
    }

    EdgeOptions getDefaultRemoteOptions() {
        return new EdgeOptions().merge(getDefaultRemoteChromiumOptions());
    }

}
