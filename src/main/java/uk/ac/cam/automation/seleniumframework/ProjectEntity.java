package uk.ac.cam.automation.seleniumframework;

import uk.ac.cam.automation.seleniumframework.properties.CommonProperties;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

public class ProjectEntity {

    public static final String getProjectName = PropertyLoader.getProperty(CommonProperties.PROJECT_NAME) != null ?
            PropertyLoader.getProperty(CommonProperties.PROJECT_NAME) :
            "AUTOMATED TEST";
}
