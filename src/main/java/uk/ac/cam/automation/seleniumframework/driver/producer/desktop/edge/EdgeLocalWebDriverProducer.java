package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.edge;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import uk.ac.cam.automation.seleniumframework.driver.producer.WebDriverProducer;

public class EdgeLocalWebDriverProducer extends BaseEdgeDriverProducer implements WebDriverProducer {

    @Override
    public WebDriver produce(){
        return new EdgeDriver(getDefaultLocalOptions());
    }

}