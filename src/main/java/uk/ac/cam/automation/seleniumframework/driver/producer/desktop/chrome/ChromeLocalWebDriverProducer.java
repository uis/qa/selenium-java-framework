package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.chrome;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import uk.ac.cam.automation.seleniumframework.driver.producer.WebDriverProducer;

public class ChromeLocalWebDriverProducer extends BaseChromeDriverProducer implements WebDriverProducer {

    @Override
    public WebDriver produce() {
        return new ChromeDriver(getDefaultLocalOptions());
    }

}
