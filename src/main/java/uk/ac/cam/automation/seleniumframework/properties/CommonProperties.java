package uk.ac.cam.automation.seleniumframework.properties;

public class CommonProperties {

    public static final String BROWSER_TYPE = "browser.type";
    public static final String VIDEO_RECORD = "browser.video.record";
    public static final String TIMEZONE = "browser.timezone";
    public static final String SCREEN_RESOLUTION = "browser.screen.resolution";
    // For BrowserStack only:
    public static final String BROWSER_PLATFORM = "browser.platform";
    public static final String BROWSER_PLATFORM_VERSION = "browser.platform.version";
    public static final String BROWSER_VERSION = "browser.version";

    public static final String PROJECT_NAME = "project.name";

    public static final String SCREENSHOT_OFF = "screenshot.off";

    public static final String SELENIUM_GRID_URL = "selenium.grid.url";
    public static final String SELENIUM_DRIVER_WAIT_TIMEOUT = "selenium.driver.wait.timeout";

    public static final String MAIL_HOST = "mail.host";
    public static final String MAIL_RETRIEVAL_PROTOCOL = "mail.retrieval.protocol";
    public static final String MAIL_RETRIEVAL_PORT = "mail.retrieval.port";
    public static final String MAIL_TIMEOUT_IN_SECONDS = "mail.timeout.seconds";

}
