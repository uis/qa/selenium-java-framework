package uk.ac.cam.automation.seleniumframework.email.domain;


import jakarta.mail.Message;

public class Recipient {
    private final String emailAddress;
    private final String name;
    private final Message.RecipientType recipientType;

    public Recipient(String emailAddress, String name) {
        this.recipientType = Message.RecipientType.TO;
        this.emailAddress = emailAddress;
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public String getEmailAddress() {
        return this.emailAddress;
    }

    public Message.RecipientType getRecipientType() {
        return this.recipientType;
    }
}
