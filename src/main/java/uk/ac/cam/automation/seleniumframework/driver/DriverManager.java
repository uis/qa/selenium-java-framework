package uk.ac.cam.automation.seleniumframework.driver;

import org.openqa.selenium.WebDriver;
import uk.ac.cam.automation.seleniumframework.log.Log;
import uk.ac.cam.automation.seleniumframework.properties.CommonProperties;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

import java.util.ArrayList;
import java.util.List;

public class DriverManager {
    private static final ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();
    private static final List<WebDriver> storedDrivers = new ArrayList<>();
    private static final BrowserType DEFAULT_BROWSER = BrowserType.ChromeLocal;

    static {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> storedDrivers.forEach(WebDriver::quit)));
    }

    public static WebDriver getDriver() {
        if (webDriver.get() == null) {
            Log.Info("Driver needs to be created, creating one...");
            createDriver();
        }

        return webDriver.get();
    }

    public static void clearDriver() {
        storedDrivers.remove(webDriver.get());
        try {
            webDriver.get().quit();
        } catch (Exception e) {
            Log.Warn("Couldn't quit one of the locally created drivers because: " + e);
        }
        webDriver.remove();
    }

    private static void createDriver() {
        String currentBrowser = PropertyLoader.getProperty(CommonProperties.BROWSER_TYPE);
        if (currentBrowser == null) {
            String fallbackBrowser = DEFAULT_BROWSER.toString();
            Log.Warn("Falling back to default of " + fallbackBrowser);
            currentBrowser = fallbackBrowser;
        }
        WebDriver d = new DriverFactory(currentBrowser).createInstance();
        storedDrivers.add(d);
        webDriver.set(d);
    }

}
