package uk.ac.cam.automation.seleniumframework.database;

import io.cucumber.java.AfterAll;
import io.cucumber.java.BeforeAll;
import org.knowm.yank.Yank;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

import java.util.Properties;

public class ConnectionManager {
    private static final String jdbcUrl = PropertyLoader.getProperty("automation.jdbc.url");
    private static final String jdbcUsername = PropertyLoader.getProperty("automation.jdbc.username");
    private static final String jdbcPassword = PropertyLoader.getProperty("automation.jdbc.password");

    @BeforeAll
    public static void initYank() {
        if (jdbcUrl != null) {
            // Connection Pool Properties
            Properties dbProps = new Properties();
            dbProps.setProperty("jdbcUrl", jdbcUrl);
            dbProps.setProperty("username", jdbcUsername);
            dbProps.setProperty("password", jdbcPassword);
            dbProps.setProperty("connectionTimeout", "100000");
            dbProps.setProperty("leakDetectionThreshold", "100000");
            dbProps.setProperty("maximumPoolSize", "20");

            // Setup connection pool
            Yank.setupDefaultConnectionPool(dbProps);
        }
    }

    @AfterAll
    public static void closeYank() {
        if (jdbcUrl != null) {
            Yank.releaseDefaultConnectionPool();
        }
    }

}
