package uk.ac.cam.automation.seleniumframework.stepdefs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import jakarta.inject.Inject;
import uk.ac.cam.automation.seleniumframework.driver.DriverManager;
import uk.ac.cam.automation.seleniumframework.guice.ScenarioScopedCache;

public class StepDefs {

    @Inject
    private ScenarioScopedCache scenarioScopedCache;

    @Given("a given step")
    public void aGivenStep() {
        DriverManager.getDriver();
        scenarioScopedCache.getString();
    }

    @When("a when step")
    public void aWhenStep() {
        scenarioScopedCache.getString();
    }

    @Then("a then step")
    public void aThenStep() {
        scenarioScopedCache.getString();
    }
}
