package uk.ac.cam.automation.seleniumframework.browser;

import org.testng.annotations.Test;
import uk.ac.cam.automation.seleniumframework.driver.DriverManager;

public class BrowserTest {
    @Test
    public void testBrowserLocalWithoutDriverBinary() {
        System.setProperty("webdriver.http.factory","jdk-http-client");
        DriverManager.getDriver().get("http://www.google.com");
        assert !DriverManager.getDriver().getWindowHandle().isEmpty();
    }
}
