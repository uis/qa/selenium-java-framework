package uk.ac.cam.automation.seleniumframework.stepdefs;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.DataProvider;

@CucumberOptions(
    glue = {
      "uk.ac.cam.automation.seleniumframework"
    },
    plugin = {"io.qameta.allure.cucumber7jvm.AllureCucumber7Jvm"},
    features = "src/test/resources/features")
public class TestRunner extends AbstractTestNGCucumberTests {
  @Override
  @DataProvider(parallel = true)
  public Object[][] scenarios() {
    return super.scenarios();
  }
}
