package uk.ac.cam.automation.seleniumframework.guice;

import jakarta.inject.Inject;
import org.testng.annotations.Test;


public class GuiceTest {
    @Inject
    private ScenarioScopedCache scenarioScopedCache;

    @Test
    public void testScenarioScopeAnnotation() {
        String string = scenarioScopedCache.getString();
    }
}
