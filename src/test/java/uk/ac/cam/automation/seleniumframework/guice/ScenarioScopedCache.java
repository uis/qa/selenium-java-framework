package uk.ac.cam.automation.seleniumframework.guice;

import java.util.Random;


public class ScenarioScopedCache {

    private String string;

    public String getString() {
        if (string == null) {
            string = randomAlphanumericToUpper(5);
        }
        return string;
    }

    private static String randomAlphanumericToUpper(int length) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            char randomChar = (char) (random.nextInt(26) + 'A'); // Generate a random uppercase letter
            sb.append(randomChar);
        }

        return sb.toString();
    }


    public void setString(String string) {
        this.string = string;
    }
}
