package uk.ac.cam.automation.seleniumframework.file;

import org.testng.Assert;
import org.testng.annotations.Test;
import uk.ac.cam.automation.seleniumframework.properties.PropertiesFileConfig;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

import java.io.IOException;


public class FileReaderTest {

    static {
        PropertyLoader.registerPropertiesFileConfig(new PropertiesFileConfig("properties/test.properties"));
    }

    @Test
    public void testReadFile() throws IOException {
        String s = FileReader.readFileFromResources("properties/test.properties");
        Assert.assertTrue(s.startsWith("test.property=test1"));
    }

}
